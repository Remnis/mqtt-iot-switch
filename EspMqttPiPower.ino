// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// MQTT Subscribes
const char* SubscribeTo = "esp/3/power/out";
const char* SubscribeFrom = "esp/3/power/in";

// WiFi & MQTT Server
const char* ssid = "********";
const char* password = "*****";
const char* mqtt_server = "192.168.178.38";

WiFiClient espClient;
PubSubClient pubClient(espClient);
long lastMsg = 0;
char msg[50];
String message("10");
String lastMessage("10");

// Mosfet
#define MOSFET_1 D2


void setup() {
  Serial.begin(115200);
  // Setting Mosfet
  pinMode(MOSFET_1, OUTPUT);
  digitalWrite(MOSFET_1, LOW);

  // WiFi
  setup_wifi();
  
  // MQTT
  Serial.print("Connecting to MQTT...");
  // connecting to the mqtt server
  pubClient.setServer(mqtt_server, 1883);
  pubClient.setCallback(callback);
  Serial.println("done!");
  
}


void loop() {
  if (!pubClient.connected()) {
    delay(100);
    reconnect();
  }
  pubClient.loop();
  
  long now = millis();
  if (now - lastMsg > 60000 || lastMessage.indexOf(message) < 0 ) {
    
    lastMsg = now;
    Serial.print("Publish message: ");
    Serial.println(message);
    char msg[7];
    message.toCharArray(msg, message.length()+1);
    Serial.print("Publishing...");
    pubClient.publish(SubscribeTo, msg);
    Serial.println("Done!!!");
    lastMessage = message;
  }
  delay(10);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String state("");
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    if(i > 0){
      state = state+ String((char)payload[i]);  
    }  
  }
  message = state;
  
  Serial.println();


  // finding payload
  if((char)payload[0] == '1'){
    // setting Mosfet 1
     if((char)payload[1] == '1'){
      // turn it on
      digitalWrite(MOSFET_1, HIGH);
      Serial.println("Turned on");
     }
     else if((char)payload[1] == '0' ){
      // turn it off
      digitalWrite(MOSFET_1, LOW);
      Serial.println("Turned off");
     }
  }
  
}


void reconnect() {
  // Loop until we're reconnected
  while (!pubClient.connected()) {
    // Attempt to connect
    if (pubClient.connect("ESP8266Client_3")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      pubClient.publish(SubscribeTo, "10");
      // ... and resubscribe
      pubClient.subscribe(SubscribeFrom);
    } else {
      Serial.print("failed, rc=");
      Serial.print(pubClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  delay(100); 
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}

